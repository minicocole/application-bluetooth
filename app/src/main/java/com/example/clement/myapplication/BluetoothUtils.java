package com.example.clement.myapplication;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import static android.support.v4.content.ContextCompat.startActivity;

public class BluetoothUtils {

    private static final String TAG = "BluetoothUtils";
    private static String Mode = "tpr";
    static private Context mContext;
    private static ConnectedThread myThread;
    private static BluetoothAdapter mBluetoothAdapter;
    private static boolean connected = false;


    public static void setContext(Context context) {
        mContext = context;
    }

    public static void setAdapter(BluetoothAdapter blt) {
        mBluetoothAdapter = blt;
    }

    public static BluetoothAdapter getmBluetoothAdapter() {
        return mBluetoothAdapter;
    }

    public static ConnectedThread getMyThread() {
        return myThread;
    }

    public static boolean isConnected(){
        return connected;
    }

    public static void setMode(String str) {
        Mode = str;
        byte[] modeMessage;
        if (connected) {
            switch (Mode) {

                case "tpr":
                    modeMessage = new byte[]{(byte) 120};
                    myThread.write(modeMessage);
                    break;
                case "graph":
                    modeMessage = new byte[]{(byte) 121};
                    myThread.write(modeMessage);
                    break;
                case "test":
                    modeMessage = new byte[]{(byte) 123};
                    myThread.write(modeMessage);
                    break;
                default:
                    break;
            }
        }
    }


    private byte[] expect = new byte[]{(byte) 255};

    class ConnectedThread extends Thread {
        public static final String SET_TEMP = BuildConfig.APPLICATION_ID + ".SET_TEMP";
        public static final String SET_HUMI = BuildConfig.APPLICATION_ID + ".SET_HUMI";
        public static final String DataPoint = BuildConfig.APPLICATION_ID + ".DataPoint";
        public static final String Display = BuildConfig.APPLICATION_ID + ".Display";
        private BluetoothSocket socketClass;
        private InputStream input;
        private OutputStream output;
        private byte[] mmbuffer;

        public ConnectedThread(BluetoothSocket socketconstruct) {
            socketClass = socketconstruct;
            try {
                input = socketClass.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                output = socketClass.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connected = true;

            switch (Mode) {
                case "tpr":
                    byte[] modeMessage = new byte[]{(byte) 120};
                    write(modeMessage);
                    break;
                case "graph":
                    modeMessage = new byte[]{(byte) 121};
                    write(modeMessage);
                    break;

                case "test":
                    modeMessage = new byte[]{(byte) 123};
                    Log.d(TAG, "Send 123+");
                    write(modeMessage);
                    break;
                default:
                    break;
            }


        }

        private int read() throws IOException {
            int readNumber = 0;
            try {
                readNumber = input.read(mmbuffer);
            } catch (IOException e) {
                connected = false;
                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(mContext, intent, new Bundle());
            }
            return readNumber;
        }

        public void run() {
            Log.d(TAG, "run: started Thread2");
            mmbuffer = new byte[1024];
            int numBytes;
            float result;
            float dec;
            while (connected) {
                try {

                    Intent tmpIntent = null;
                    numBytes = read();
                    Log.d(TAG, "received : " + numBytes);
                    switch (numBytes) {
                        case 111:
                            Log.d(TAG, "111");
                            tmpIntent = new Intent(SET_TEMP);
                            write(expect);
                            numBytes = read();
                            write(expect);
                            dec = read() % 255;
                            write(expect);

                            result = numBytes + dec / 10f;
                            Log.d(TAG, "received :" + numBytes + ' ' + dec / 10f);
                            tmpIntent.putExtra("value", result);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(tmpIntent);

                            break;
                        case 112:
                            Log.d(TAG, "112");
                            tmpIntent = new Intent(SET_HUMI);
                            write(expect);
                            numBytes = read();
                            write(expect);
                            dec = read() % 255;
                            write(expect);
                            result = numBytes + dec / 10f;
                            Log.d(TAG, "received :" + numBytes + ' ' + dec / 10f);
                            tmpIntent.putExtra("value", result);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(tmpIntent);

                            break;
                        case 113:
                            Log.d(TAG, "113");
                            tmpIntent = new Intent(DataPoint);
                            write(expect);
                            numBytes = read();
                            if (!(numBytes <= 100 && numBytes >= 0)) break;
                            int annee1 = numBytes;
                            write(expect);
                            numBytes = read();
                            int annee2 = numBytes;
                            if (!(numBytes <= 100 && numBytes >= 0)) break;
                            write(expect);
                            int annee = annee1 * 100 + annee2;
                            Log.d(TAG, "found : " + annee);
                            tmpIntent.putExtra("year", annee);

                            numBytes = read();
                            if (!(numBytes <= 100 && numBytes >= 0)) break;
                            int mois = numBytes;
                            write(expect);
                            Log.d(TAG, "found : " + mois);
                            tmpIntent.putExtra("month", mois);

                            numBytes = read();
                            if (!(numBytes <= 100 && numBytes >= 0)) break;
                            int jour = numBytes;
                            Log.d(TAG, "found : " + jour);
                            tmpIntent.putExtra("day", jour);
                            write(expect);

                            numBytes = read();
                            if (!((numBytes <= 100 && numBytes >= 0) || numBytes==255)) break;
                            int hour = numBytes%255;
                            Log.d(TAG, "found : " + hour);
                            tmpIntent.putExtra("hour", hour);
                            write(expect);

                            numBytes = read();
                            if (!((numBytes <= 100 && numBytes >= 0) || numBytes==255)) break;
                            int minute = numBytes%255;
                            Log.d(TAG, "found : " + minute);
                            tmpIntent.putExtra("minute", minute);
                            write(expect);
                            Log.d(TAG, "Date :" + annee + '-' + mois + '-' + jour + ' ' + hour + ':' + minute);

                            numBytes = read();
                            if (!(numBytes <= 100 && numBytes >= 0)) break;
                            int tempent = numBytes;
                            write(expect);
                            numBytes = read() % 255;
                            if (!(numBytes <= 100 && numBytes >= 0)) break;
                            int tempdec = numBytes;
                            float temperature = tempent + (tempdec / 10f);
                            tmpIntent.putExtra("temp", temperature);
                            write(expect);

                            numBytes = read();
                            if (!(numBytes <= 100 && numBytes >= 0)) break;
                            int humient = numBytes;
                            write(expect);
                            numBytes = read() % 255;
                            if (!(numBytes <= 100 && numBytes >= 0)) break;
                            int humidec = numBytes;
                            float humidite = humient + (humidec / 10f);
                            tmpIntent.putExtra("humi", humidite);
                            write(expect);


                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(tmpIntent);


                            break;

                        case 114:


                            Log.d(TAG, "114");
                            tmpIntent = new Intent(Display);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(tmpIntent);
                        default:
                            Log.d(TAG, "received : " + numBytes);
                            break;
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }


        public void write(byte[] bytes) {
            try {
                output.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;


            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.d(TAG, "run: started Thread 1");
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            myThread = new ConnectedThread(mmSocket);
            myThread.start();

        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }
}

