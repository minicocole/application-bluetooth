package com.example.clement.myapplication;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class GraphActivity extends AppCompatActivity {
    private static final String TAG = "GraphActivity";
    LineGraphSeries<DataPoint> seriesTemp;
    LineGraphSeries<DataPoint> seriesHumi;
    GraphView graph;
    BroadcastReceiver broad = null;
    ProgressBar progressBar;
    TextView text;
    Context mContext = this;
    Object listeDate = new Object();

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        Toolbar toolbar = findViewById(R.id.toolbargraph);
        setSupportActionBar(toolbar);
        progressBar = findViewById(R.id.progressBar);
        text = findViewById(R.id.textView);
        ActionBar ab = getSupportActionBar();

        ab.setDisplayHomeAsUpEnabled(true);


        graph = (GraphView) findViewById(R.id.graph);

// you can directly pass Date objects to DataPoint-Constructor
// this will convert the Date to double via Date#getTime()

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_graph, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.graph) {
            Intent intent = new Intent(this, GraphActivity.class);
            startActivity(intent);
            return true;

        }

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
        graph.setVisibility(GraphView.INVISIBLE);
        text.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        graph.removeAllSeries();
        BluetoothUtils.setContext(this);
        final TreeMap<Date, ArrayList<Float>> table = new TreeMap<Date, ArrayList<Float>>();

        Log.d(TAG, ""+seriesHumi);
        this.seriesTemp = new LineGraphSeries<>();
        this.seriesHumi = new LineGraphSeries<>();

        BluetoothUtils.setMode("graph");
        if(broad == null){
            broad = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    String action = intent.getAction();
                    switch (action) {

                        case BluetoothUtils.ConnectedThread.DataPoint :
                            Bundle args = intent.getExtras();
                            GregorianCalendar calendar = new GregorianCalendar(args.getInt("year"), args.getInt("month")-1, args.getInt("day"), args.getInt("hour"), args.getInt("minute"));
                            Date date = calendar.getTime();
                            Log.d(TAG, date.toString());
                            ArrayList<Float> array = new ArrayList<Float>();
                            array.add(args.getFloat("temp"));
                            array.add(args.getFloat("humi"));
                            table.put(date, array);


                            break;
                        case BluetoothUtils.ConnectedThread.Display:
                            Log.d(TAG, "onReceive: acquired");
                            Log.d(TAG, table.toString());

                            Set set = table.entrySet();
                            // Get an iterator
                            Iterator i = set.iterator();
                            // Display elements
                            while(i.hasNext()) {
                                Map.Entry me = (Map.Entry)i.next();
                                Log.d(TAG, me.getKey().toString());
                                Log.d(TAG, me.getValue().toString());
                                Date localdate = (Date)me.getKey();
                                ArrayList<Float> localarray = (ArrayList<Float>) me.getValue();
                                Float localtemp = localarray.get(0);
                                Float localhumi = localarray.get(1);
                                Log.d("DateAdd", "adding Date : "+localdate);
                                seriesTemp.appendData(new DataPoint(localdate, localtemp), false, 1000, false);
                                seriesHumi.appendData(new DataPoint(localdate, localhumi), false, 1000, false);

                            }



                            seriesHumi.setTitle("Humidité");
                            seriesTemp.setTitle("Température");
                            seriesHumi.setColor(Color.BLUE);
                            seriesTemp.setColor(Color.RED);
                            text.setVisibility(TextView.INVISIBLE);
                            progressBar.setVisibility(View.INVISIBLE);
                            graph.addSeries(seriesTemp);
                            graph.addSeries(seriesHumi);

                            graph.setVisibility(View.VISIBLE);

                            graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(mContext, new SimpleDateFormat("dd-M-yyyy HH:mm")));
                            graph.getGridLabelRenderer().setNumHorizontalLabels(3);
                            graph.getLegendRenderer().setVisible(true);

                            break;
                        default:
                            break;

                    }




                }
            };
            Log.d(TAG, "onResume: Did it");
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothUtils.ConnectedThread.DataPoint);
            filter.addAction(BluetoothUtils.ConnectedThread.Display);

            LocalBroadcastManager.getInstance(this).registerReceiver(broad, filter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broad);
    }
}






