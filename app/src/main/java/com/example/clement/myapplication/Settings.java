package com.example.clement.myapplication;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;


public class Settings extends AppCompatActivity {

    private static final String TAG = "Settings";
    EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbarsettings);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();

        ab.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        BluetoothUtils.setMode("test");
        BluetoothUtils.setContext(this);

    }


    public void Valider(View view) throws InterruptedException {

            BluetoothUtils.getMyThread().write(new byte[]{(byte)125});



    }

    public void Blue(View view) {
        BluetoothUtils.getMyThread().write(new byte[]{(byte)128});
    }

    public void Red(View view) {
        BluetoothUtils.getMyThread().write(new byte[]{(byte)127});
    }

    public void Green(View view) {
        BluetoothUtils.getMyThread().write(new byte[]{(byte)126});
    }
}

