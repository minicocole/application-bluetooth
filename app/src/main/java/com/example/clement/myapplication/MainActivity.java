package com.example.clement.myapplication;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_ENABLE_BT = 1;
    private static final String TAG = "Bug";
    public TextView mtext;
    public TextView mtext2;
    BluetoothUtils.ConnectedThread myThread;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothDevice mdevice = null;
    BluetoothSocket mmSocket;
    float value;
    BluetoothUtils.ConnectThread initialThread;
    BroadcastReceiver broad = null;

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void setText(String text) {
        mtext.setText(text);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mtext = findViewById(R.id.edit);
        mtext2 = findViewById(R.id.edit2);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (BluetoothUtils.getmBluetoothAdapter() != mBluetoothAdapter) {
            BluetoothUtils.setAdapter(mBluetoothAdapter);
        }


    }


    @Override
    protected void onResume() {
        super.onResume();


        BluetoothUtils.setContext(this);
        BluetoothUtils.setMode("tpr");

        if (broad == null) {
            broad = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    String action = intent.getAction();
                    switch (action) {


                        case BluetoothUtils.ConnectedThread.SET_TEMP:
                            value = intent.getFloatExtra("value", 0);
                            mtext.setText(Float.toString(value));
                            break;
                        case BluetoothUtils.ConnectedThread.SET_HUMI:
                            value = intent.getFloatExtra("value", 0);
                            mtext2.setText(Float.toString(value));
                            break;
                    }


                }
            };
            Log.d(TAG, "onResume: Did it");
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothUtils.ConnectedThread.SET_TEMP);
            filter.addAction(BluetoothUtils.ConnectedThread.SET_HUMI);

            LocalBroadcastManager.getInstance(this).registerReceiver(broad, filter);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (BluetoothUtils.isConnected()) {
            //noinspection SimplifiableIfStatement
            if (id == R.id.graph) {
                Intent intent = new Intent(this, GraphActivity.class);
                startActivity(intent);
                return true;
            }
            if (id == R.id.action_settings) {
                Intent intent = new Intent(this, Settings.class);
                startActivity(intent);
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
        Toast.makeText(this,"Connect First",Toast.LENGTH_SHORT).show();
        return true;
    }

    public void Clicked(View view) {

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        mtext.setText("Connecting...");
        if (initialThread != null) {
            initialThread.cancel();
        }

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();

                if (deviceName.equals("E-Nest")) {
                    mdevice = device;
                    initialThread = new BluetoothUtils().new ConnectThread(mdevice);
                    initialThread.start();
                    break;
                }
            }
        }
        if (mdevice == null) {
            mtext.setText("Pair device");
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broad);
    }
}








